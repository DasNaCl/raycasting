/*
    Map
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAP_HPP
#define MAP_HPP

#include <map>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Image.hpp>
#include <string>
#include <memory>

class Map
{
public:
    Map(const sf::Vector2u& size_); //size -> blocks

    bool loadFromFile(const std::string& file);
    sf::Vector2u size();
    unsigned int operator()(unsigned int index);
    unsigned int& operator[](unsigned int index);

    sf::Vector2u getWallDataSize(unsigned int wallcode);
    const sf::Uint8* getWallData(unsigned int wallcode);
    void setColor(unsigned int wallcode, sf::Color col);
    void setWallData(unsigned int wallcode, const sf::Uint8* data, sf::Vector2u __size); //format must be ((x + y * width) * 4)

private:
    typedef std::pair<const sf::Uint8*, sf::Vector2u> MapWallData;
private:
    std::map<unsigned int, unsigned int> map; // index - id     index is like (x + y * width)
    std::map<unsigned int, MapWallData> walldata;
    sf::Vector2u _size;
    std::map<unsigned int, sf::Uint8[4]> imgdata; //just to keep the colors, its uneffective, I know
};

#endif
