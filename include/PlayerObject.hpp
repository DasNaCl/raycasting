/*
    "camera"
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PLAYEROBJECT_HPP_INCLUDED
#define PLAYEROBJECT_HPP_INCLUDED

#include <SFML/System/Vector2.hpp>

class PlayerObject
{
public:
    PlayerObject();
    virtual ~PlayerObject();

    sf::Vector2f direction();
    sf::Vector2f camplane();
    sf::Vector2f position();

    void setPosition(sf::Vector2f _pos);
    void setPosition(float x, float y);

protected:
    //no direction-vector needed, just apply the transform and make the length of pos bigger

    sf::Vector2f dir; // direction
    sf::Vector2f plane; // 2d camera plane
    sf::Vector2f pos;

private:

};

#endif // PLAYEROBJECT_HPP_INCLUDED
