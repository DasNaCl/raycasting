/*
    Renders the scene
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RENDERER_HPP_INCLUDED
#define RENDERER_HPP_INCLUDED

#include "Map.hpp"
#include "PlayerObject.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <vector>

class Renderer : public sf::Drawable, public sf::Transformable
{
public:
    Renderer(const Map& _map, PlayerObject& player, const sf::Vector2u& size); //size -> pixels
    //wallheight is initially size.y

    void setWallHeight(unsigned int _height);
    void draw(sf::RenderTarget& target, sf::RenderStates states) const;

public:
    void update() const;

private:
    mutable sf::Sprite spr;
    mutable sf::Texture tex;
    mutable std::vector<sf::Uint8> pixels;
    mutable Map map;
    PlayerObject& playerobj;
    int height;
};

#endif // RENDERER_HPP_INCLUDED
