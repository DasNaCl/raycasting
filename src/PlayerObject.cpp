/*
    "camera"
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/PlayerObject.hpp"

PlayerObject::PlayerObject() : dir(-1.f, 0.f), plane(0.66f, 0.66f), pos(1.5f, 1.5f)
{

}

PlayerObject::~PlayerObject()
{

}

void PlayerObject::setPosition(sf::Vector2f _pos)
{
    pos = _pos;
}

void PlayerObject::setPosition(float x, float y)
{
    pos = sf::Vector2f(x, y);
}

sf::Vector2f PlayerObject::position()
{
    return pos;
}

sf::Vector2f PlayerObject::camplane()
{
    return plane;
}

sf::Vector2f PlayerObject::direction()
{
    return dir;
}
