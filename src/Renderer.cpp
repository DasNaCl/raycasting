/*
    Renders the scene
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Renderer.hpp"
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Texture.hpp>

#ifndef NDEBUG
#include <iostream>
#endif

Renderer::Renderer(const Map& _map, PlayerObject& player, const sf::Vector2u& size)
    : spr(), tex(), pixels(), map(_map), playerobj(player), height(static_cast<int>(size.y))
{
    pixels.resize(size.x * size.y * 4);
    if(!tex.create(size.x, size.y))
        throw std::string("Texture size is invalid.");
}

void Renderer::setWallHeight(unsigned int _height)
{
    height = _height;
}

void Renderer::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    update();

    sf::Uint8* ar = &pixels[0];
    tex.update(ar);
    spr.setTexture(tex);

    states.transform *= getTransform();

    target.draw(spr, states);

    pixels.clear();
    pixels.resize(tex.getSize().x * tex.getSize().y * 4, 255);
}

void Renderer::update() const
{
    for(unsigned int x = 0; x < tex.getSize().x; ++x)
    {
        double camX = (2 * x)/(static_cast<double>(tex.getSize().x - 1));
        ///raypos is playerpos
        sf::Vector2<double> rayDir(playerobj.direction() + sf::Vector2f(playerobj.camplane().x * camX, playerobj.camplane().y * camX));

        sf::Vector2i mapPos(static_cast<sf::Vector2i>(playerobj.position()));

        //what direction to step in x or y-direction (either +1 or -1)
        sf::Vector2i step;
        //length of ray from current position to next x or y-side
        sf::Vector2<double> sideDist;
        //length of ray from one x or y-side to next x or y-side
        sf::Vector2<double> deltaDist(std::sqrt(1+(rayDir.y*rayDir.y) / (rayDir.x*rayDir.x)), std::sqrt(1+(rayDir.x*rayDir.x) / (rayDir.y*rayDir.y)));
        double perpWallDist=0.;

        if(rayDir.x<0)
        {
            step.x = -1;
            sideDist.x = (playerobj.position().x-mapPos.x) * deltaDist.x;
        }
        else
        {
            step.x = 1;
            sideDist.x = (mapPos.x + 1.0 - playerobj.position().x) * deltaDist.x;
        }
        if(rayDir.y<0)
        {
            step.y = -1;
            sideDist.y = (playerobj.position().y-mapPos.y) * deltaDist.y;
        }
        else
        {
            step.y = 1;
            sideDist.y = (mapPos.y + 1.0 - playerobj.position().y) * deltaDist.y;
        }

        //DDA
        int whathit = 0;
        bool northsouth = false;
        while(whathit == 0)
        {
            if(sideDist.x < sideDist.y)
            {
                sideDist.x += deltaDist.x;
                mapPos.x += step.x;
                northsouth = false;
            }
            else
            {
                sideDist.y += deltaDist.y;
                mapPos.y += step.y;
                northsouth = true;
            }

            if((mapPos.x+mapPos.y*map.size().x) < map.size().x*map.size().y)
                whathit = map(mapPos.x + mapPos.y * map.size().x);
            else
                whathit = 1;
        }
        //Calculate distance projected on camera direction
        if(!northsouth)
        {
            perpWallDist = std::fabs((mapPos.x - playerobj.position().x + (1 - step.x) / 2) / rayDir.x);
        }
        else
        {
            perpWallDist = std::fabs((mapPos.y - playerobj.position().y + (1 - step.y) / 2) / rayDir.y);
        }

        int line = std::abs(static_cast<int>(height / perpWallDist));

        sf::Vector2i drawStartEnd(-line / 2 + height / 2, line / 2 + height / 2);

        if(drawStartEnd.x < 0)
            drawStartEnd.x = 0;
        if(drawStartEnd.y >= height)
            drawStartEnd.y = height-1;

        float multiplier = ((northsouth)?(1.f):(.5f));
        int wallcode = map[mapPos.x+mapPos.y*map.size().x];
        try
        {
            double wallX = ((northsouth)
                            ?
                            (playerobj.position().x + ((mapPos.y - playerobj.position().y + (1 - step.y) / 2.f) / rayDir.y) * rayDir.x)
                            :
                            (playerobj.position().y + ((mapPos.x - playerobj.position().x + (1 - step.x) / 2.f) / rayDir.x) * rayDir.y)
                            );

            wallX -= std::floor(wallX);
            sf::Vector2u imgsize(map.getWallDataSize(wallcode));
            sf::Vector2i texCoords;
            texCoords.x = static_cast<int>(wallX * static_cast<double>(imgsize.x));

            if((!northsouth && rayDir.x > 0.) || (northsouth && rayDir.y < 0.))
                texCoords.x = imgsize.x - texCoords.x - 1;

            for(int y = drawStartEnd.x; y < drawStartEnd.y; ++y)
            {
                int d = y * 256 - height * 128 + line * 128;
                texCoords.y = ((d * imgsize.y) / line) / 256;
                const sf::Uint8* pxs = &(map.getWallData(wallcode))[(texCoords.x + texCoords.y * imgsize.x) * 4];
                int index = (x + y * tex.getSize().x) * 4;
                pixels[index + 0] = pxs[0] * multiplier;
                pixels[index + 1] = pxs[1] * multiplier;
                pixels[index + 2] = pxs[2] * multiplier;
                pixels[index + 3] = pxs[3] * multiplier;
            }
        }
        catch(std::string error)
        {
            #if !defined NDEBUG
            std::cerr << "Error in Renderer: " << error << "\n";
            #endif
        }
    }
}
