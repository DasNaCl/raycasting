/*
    Map
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Map.hpp"
#include <fstream>
#include <sstream>
#include <initializer_list>

Map::Map(const sf::Vector2u& size_) : map(), walldata(), _size(size_), imgdata()
{
    for(unsigned int x=0U; x<_size.x; ++x)
    {
        for(unsigned int y=0U; y<_size.y; ++y)
        {
            map[x + _size.x * y] = 0U; //0U is everytime ground.
        }
    }
}

sf::Vector2u Map::getWallDataSize(unsigned int wallcode)
{
    auto it = walldata.find(wallcode);

    if(it != walldata.end())
        return it->second.second;

    return sf::Vector2u();
}

const sf::Uint8* Map::getWallData(unsigned int wallcode)
{
    auto it = walldata.find(wallcode);

    if(it != walldata.end())
        return it->second.first;

    return nullptr;
}

void Map::setColor(unsigned int wallcode, sf::Color col)
{
    auto it = walldata.find(wallcode);
    if(it == walldata.end())
    {
        auto init = std::initializer_list<sf::Uint8>({col.r, col.g, col.b, col.a});
        std::copy(init.begin(), init.end(), imgdata[wallcode]);

        walldata.insert(std::map<unsigned int, MapWallData>::value_type(wallcode, MapWallData(imgdata[wallcode],
                                                                                  sf::Vector2u(1U, 1U))));
    }
    else
    {
        std::stringstream ss;
        ss << wallcode;
        throw std::string("Wallcode " + ss.str() + " has already data.");
    }
}

void Map::setWallData(unsigned int wallcode, const sf::Uint8* data, sf::Vector2u __size)
{
    auto it = walldata.find(wallcode);
    if(it == walldata.end())
    {
        if(data == nullptr)
        {
            std::stringstream ss;
            ss << wallcode;
            throw std::string("Data for wallcode " + ss.str() + " is corrupted.");
        }
        walldata.insert(std::map<unsigned int, MapWallData>::value_type(wallcode, MapWallData(data, __size)));
    }
    else
    {
        std::stringstream ss;
        ss << wallcode;
        throw std::string("Wallcode " + ss.str() + " has already data.");
    }
}

bool Map::loadFromFile(const std::string& filename)
{
    //TO DO: Load colors.

    std::fstream f(filename, std::ios::in);
    if(!f)
        return false;

    std::string input;
    bool first=true;
    unsigned int y=0U;
    while(std::getline(f, input))
    {
        if(first)
        {
            std::stringstream iss(input);
            if(!(iss >> _size.x >> _size.y) || _size.x < 3U || _size.y < 3U)
            {
                throw std::string("Invalid file! You need to define a size.");
            }

            first = false;
        }
        else
        {
            std::stringstream iss(input);
            for(unsigned int x=0U; x<_size.x; ++x)
            {
                if(!(iss >> map[x + y * _size.x]))
                {
                    throw std::string("Invalid file! Data is corrupted");
                }
            }
            ++y;
        }
    }
    if(y != _size.y)
        throw std::string("Invalid file! Not enough data");

    return !first;
}

sf::Vector2u Map::size()
{
    return _size;
}

unsigned int Map::operator()(unsigned int index)
{
    return map[index];
}

unsigned int& Map::operator[](unsigned int index)
{
    return map[index];
}
